import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';
import 'image_objects.dart';
import 'dart:math' as math;
import 'package:syncfusion_flutter_gauges/gauges.dart';

class EditPage extends StatefulWidget {
  final index;

  EditPage(this.index);

  _EditPage createState() => _EditPage();
}

bool flip = false;
bool scale = false;
bool transform = false;
bool translate = false;

class _EditPage extends State<EditPage> {
  var index;
  double sliderVal = 0.5;
  double rotateSize = 0;
  double x = 0;
  double y = 0;
  double z = 0;
  double markerPoint = 0;

  @override
  void initState() {
    super.initState();
    index = widget.index;
    sliderVal = 0.5;
  }

  void onSizeChanged(double value) {
    setState(() {
      rotateSize = value;
    });
  }

  SliderTheme slider() {
    return SliderTheme(
      data: SliderThemeData(
          thumbColor: Colors.green,
          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 5)),
      child: Slider(
          value: sliderVal,
          min: .5,
          max: 1,
          onChanged: (val) {
            setState(() {
              sliderVal = val;
            });
          }),
    );
  }

  SfRadialGauge slider2() {
    return SfRadialGauge(
        title: GaugeTitle(text: 'Angle', textStyle: TextStyle(
          fontSize: 20.0, fontWeight: FontWeight.bold, ),
            alignment: GaugeAlignment.center),
        axes: <RadialAxis>[
          RadialAxis(
              showTicks: false,
              showLabels: false,
              radiusFactor: .4,
              startAngle: 270,
              endAngle: 270,
              minimum: 0,
              interval: 30,
              pointers: <GaugePointer>[
                MarkerPointer(
                  value: markerPoint,
                    enableDragging: true,
                    onValueChanged: onSizeChanged,
                    markerHeight: 34,
                    markerWidth: 34,
                    markerType: MarkerType.circle,
                    color: Color(0xFF753A88),
                    borderWidth: 2,
                    borderColor: Colors.white54)
              ])
        ]);
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Page'),
      ),
      body: ListView(children: [
        Center(
          child: Transform.rotate(
              angle: rotateSize,
              child: Container(
                  child: Transform.scale(
                      scale: sliderVal,
                      child: Container(
                          child: Transform(
                              transform: Matrix4(
                                1,
                                0,
                                0,
                                0,
                                0,
                                1,
                                0,
                                0,
                                0,
                                0,
                                1,
                                0,
                                0,
                                0,
                                0,
                                1,
                              )
                                ..rotateX(x)
                                ..rotateY(y)
                                ..rotateZ(z),
                              alignment: FractionalOffset.center,
                              child: GestureDetector(
                                  onPanUpdate: (details) {
                                    setState(() {
                                      y = y - details.delta.dx / 100;
                                      x = x + details.delta.dy / 100;
                                    });
                                  },
                                  child: Container(
                                      child: Center(
                                    child: Image.asset(images[index].filename),
                                  )))
                              // child: Container(
                              //   child: Center(
                              //     child: Image.asset(images[index].filename),
                              ))))),
        ),
        Container(
          child: Text('Size',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center),
        ),
        slider(),
        slider2(),
      ]),
      floatingActionButton: FloatingActionButton.extended(
        label: Text('Reset'),
        onPressed: (){
          setState(() {
            sliderVal = 0.5;
            rotateSize = 0;
            x = 0;
            y = 0;
            z = 0;
            markerPoint = 20;
          });
        },
      )
    );
  }
}
