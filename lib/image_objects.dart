import 'package:flutter/material.dart';


class Images{
  final String filename;
  // TODO: add description and user if we can expand users
  //final String description;
  //final String user;

  Images({this.filename});
}

var images = <Images>[
  Images(filename: "assets/images/picture1.jpg"),
  Images(filename: "assets/images/picture2.jpg"),
  Images(filename: "assets/images/picture3.jpg"),
  Images(filename: "assets/images/picture4.jpg"),
  Images(filename: "assets/images/picture5.jpg"),
  Images(filename: "assets/images/picture6.jpg"),
  Images(filename: "assets/images/picture7.jpg"),
  Images(filename: "assets/images/picture8.jpg"),
  Images(filename: "assets/images/picture9.jpg"),
  Images(filename: "assets/images/picture10.jpg"),
];