import 'package:flutter/material.dart';
import 'image_objects.dart';

// ignore: must_be_immutable
class ImageDialog extends StatelessWidget {
  int index;
  ImageDialog(this.index);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Stack(
        children: <Widget>[
          Container(
            width: 500,
            height: 500,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage(images[index].filename),
                  fit: BoxFit.contain,
                )
            ),
          ),
          Container(
            width: 500,
            height: 100,
            alignment: Alignment.topRight,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {  },
                ),
                IconButton(
                  icon: Icon(Icons.download_rounded),
                  onPressed: () {  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
