import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'image_objects.dart';
import 'EditPage.dart';

class DetailPage extends StatelessWidget {
  final int index;
  DetailPage(this.index);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Image')),
      body: PageTransitionSwitcher(
        transitionBuilder: (
            Widget child,
            Animation<double> animation,
            Animation<double> secondaryAnimation
            ){
          return FadeThroughTransition(
            animation: animation,
            secondaryAnimation: secondaryAnimation,
            child: child,
          );
        },
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.grey,
              child: Row(
                children: [
                  IconButton(
                      icon: Icon(Icons.edit),
                      tooltip: 'Edit',
                      onPressed:(){
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => EditPage(index)));
                      }
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                width: 500,
                height: 500,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    image: DecorationImage(
                      image: ExactAssetImage(images[index].filename),
                      fit: BoxFit.contain,
                    )
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
