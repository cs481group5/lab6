//
// Group 5: CJ Trujillo, Ian Altoveros, Gintas Kazlauskas, Howard Tep
// 11/17/20
// CS 481 Lab 6
// Container Transform, Fade Through, & Progress Indicators
//
// Citation: api.flutter.dev
//    images- freeimages.com
//    image dialog- https://stackoverflow.com/questions/60047676/flutter-display-photo-or-image-as-modal-popup
//    fade through- https://blog.logrocket.com/introducing-flutters-new-animations-package/
//    image border- https://stackoverflow.com/questions/51513429/rounded-corners-image-in-flutter
import 'package:cs481_lab6/detail.dart';
import 'package:flutter/material.dart';
import 'image_objects.dart';
import 'dart:async';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
        home: SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Images"),
      ),
      backgroundColor: Colors.grey,
      body: Container(                      //builds grid
        padding: EdgeInsets.all(16),
        child: GridView.builder(
          itemCount: images.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3,
              crossAxisSpacing: 2, mainAxisSpacing: 2),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(       //onTap for each picture
              onTap: () async {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DetailPage(index)));
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  images[index].filename,
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class SplashScreen extends StatefulWidget{
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Color> _colorTween;
  double download = 0.0;

  @override
  void initState(){
    _animationController = AnimationController(
      duration: Duration(seconds: 6),
      vsync: this,
    );
    _colorTween = _animationController.drive(
      ColorTween(
        begin: Colors.red,
        end: Colors.blue));
    _animationController.repeat();
    super.initState();
    Timer(Duration(seconds: 5), () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyHomePage())));
  }

  @override
  void update(){
    Timer.periodic(Duration(milliseconds: 540), (timer) {
      download = download + .01;
      if(!mounted) return;
      setState(() {
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    update();
    return Container(
      child: Stack(
          children:[
            Positioned(
              bottom: 300,
              right: 100,
              child: SizedBox(
                child: CircularProgressIndicator(
                  valueColor: _colorTween,
                  strokeWidth: 20,
                  backgroundColor: Colors.white10,
                ),
                height: 200,
                width: 200,
              ),
            ),
            Positioned(
              bottom: 200,
              child: SizedBox(
                child: LinearProgressIndicator(
                  backgroundColor: Colors.white10,
                  valueColor: AlwaysStoppedAnimation(Colors.green),
                  value: download,
                ),
                height: 20,
                width: 400,
              ),
            )
          ],
      ),
    );
  }
  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

}




